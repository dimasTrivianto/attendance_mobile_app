import { IonContent,IonPage, IonTitle, IonList, IonItem, IonInput, IonButton, IonIcon, IonImg} from '@ionic/react';
import React, {useState, useRef} from 'react';
// import ExploreContainer from '../components/ExploreContainer';
import './Login.css';
import loginImg from '../assets/yokesen_logo.svg';
import { logInOutline } from 'ionicons/icons';
import axios from '../config/axios';
// import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { loginAction } from '../config/redux/actions'
import { Redirect } from 'react-router-dom';


// console.log(typeof(loginImg))
// type Item = {
//   src: string;
//   text: string;
// }

// const items: Item[] =[{src: loginImg, text: 'logo Yokesen'}]
// const items: any = loginImg
const items: string = loginImg

const Login: React.FC = () => {
  const [text, setText] = useState<string>();
  const [password, setPassword] = useState<string>();
  // const [token, setToken] = useState<string>()
  // <HTMLIonInputElement> digunakan untuk memberi tahu typescript element mana useRef akan digunakan dan set nilainya ke null karena pada saat initialize belum ada isinya
  const emailRef = useRef<HTMLIonInputElement>(null)
  const passwordRef = useRef<HTMLIonInputElement>(null)

  const dispatch = useDispatch()

  // interface RootState {
  //   email: string
  // }

  // const emails = (state: RootState) => state.email

  const emailUser = useSelector((state: any) => state.email)

  const login = () =>{
    // tanda ? di current adalah syntax tyoescript untuk mengecek apakah current ada isinya sebelum mengecek isi value (seperti ternary operator dalam javascript biasa)
    // const email = emailRef.current?.value;
    // const password = passwordRef.current?.value;

    // ? bisa diganti dengan ! jika kita yakin bahwa pada saat conection terjadi nilainya tidak akan null, dan dalam case ini pasti tidak akan null karena akan selalu terhubung dengan ref
    const email = emailRef.current!.value;
    const password = passwordRef.current!.value;
    const data = {
      email,
      password 
    }
    
    axios.post('/api/login', data)
    .then( res => {
      // console.log(res.data)
      dispatch(loginAction(res.data))

    })
    .catch(err => console.log({err}))

  }

  return (
    <IonPage className="backgroundColor">
      { ! emailUser ?
        <IonContent fullscreen>
        <IonList>
          {/* {items.map((image, i) => (
              <img className="img-login" src={image.src} alt="" key={i}/>            
          ))} */}
          <IonImg className="img-login" src={items} alt="logo-yokesen"/>
        </IonList>
        <IonList>
          <IonItem  lines="none" className="title">
            <IonTitle className="backgroundColor ion-text-center">Welcome to Zukses</IonTitle> 
          </IonItem>
          <IonItem lines="none" className="input-box">
            <IonInput type="email" value={text} placeholder="Email" onIonChange={e => setText(e.detail.value!)} ref={emailRef}></IonInput>
          </IonItem>
          <IonItem lines="none" className="input-box">
            <IonInput type="password" value={password} placeholder="password" onIonChange={e => setPassword(e.detail.value!)} ref={passwordRef}></IonInput>
          </IonItem>
          <IonItem lines="none">
            <IonButton className="login-button"  shape="round" onClick={login}>
              <IonIcon slot="start" icon={logInOutline} />
              Login
            </IonButton>
          </IonItem>
        </IonList>
      </IonContent> 
        :
      <Redirect to="/" />
      } 
    </IonPage>
  );
};

export default Login;
