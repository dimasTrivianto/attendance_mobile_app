import React, {useState, useEffect} from 'react'
import { IonGrid, IonRow, IonLabel, IonCard, IonCardContent, IonCardHeader, IonList, IonItem, IonButton, IonIcon, IonAlert, IonCol, IonPage, IonContent} from '@ionic/react'; 
import { useSelector, useDispatch } from 'react-redux';
import axios from '../config/axios';
import { Redirect } from 'react-router-dom';
import { tapOut, tapIn, onLogout } from '../config/redux/actions'
import { logOutOutline } from 'ionicons/icons';

import './Login.css';



const Home: React.FC = () => {

  const [clock, setclock] = useState<Date>(new Date())
  const emailUser = useSelector((state: any) => state.email)
  const token = useSelector((state: any) => state.token)
  const [color, setcolor] = useState<string>('gray')
  const [clockColor, setclockColor] = useState<string>('clockCardGray')
  const [tap, settap] = useState<boolean>(false)
  const attendance = useSelector((state: any) => state.attendance)
  const [absence, setabsence] = useState<string>(attendance)
  const [showAlert, setShowAlert] = useState(false);
  const [maxClockIn, setmaxClockIn] = useState<boolean>(false);
  const dispatch = useDispatch()

  const tick = () => {
    setclock(new Date())
  }

  useEffect(() => {
    let intervalID = setInterval(() => tick(),
    1000
    );
    return () => {
      clearInterval(intervalID);
    }
  }, []);

  const tapClock = () => {
    
    const config = {
      headers: {
        'Authorization' : 'Bearer'+' '+ token
      }
    }

    const body = {
      email: emailUser
    }

    if(absence == 'true'){
      setcolor('green');
      setclockColor('clockCardGreen')
      settap(true)
      setabsence('false')

      return axios.post('/api/clock-out', body, config)
      .then(res => {
        let intervalID = setTimeout(() => {
          setcolor('gray');
          setclockColor('clockCardGray')
          settap(false)
          
        },
        1000
        );
        
        const absence = 'false'
        dispatch(tapOut({attendance : absence}));
        console.log(res.data)
        
        return () => clearTimeout(intervalID);
      })
      .catch(err => console.log({err}))
    }else{
      
      return axios.post('/api/clock-in', body, config)
      .then(res => {
        if(res.data.message == "messages.maxColckIn"){
          setmaxClockIn(true)

        } else{

          setcolor('green');
          setclockColor('clockCardGreen')
          settap(true)
          setabsence('true')

          let intervalID = setTimeout(() => {
            setcolor('gray');
            setclockColor('clockCardGray')
            settap(false)
          },
          10000
          );
          const absence = 'true'
          dispatch(tapIn({attendance : absence}));
          console.log(res.data)
  
          return () => clearTimeout(intervalID);
        }
        
      })
      .catch(err => console.log({err}))
    }
  }

  const logOut = () => {
    dispatch(onLogout())
  }


  return(
      <IonPage>
        <IonContent>
            <IonButton className="logout" onClick={logOut}>
              <IonIcon slot="icon-only" icon={logOutOutline} color="danger"></IonIcon>
            </IonButton>
          { emailUser ?
            <IonGrid>
              <IonRow ion-align-items-center justify-content-center >
                <IonCol size='12' ion-text-center ion-align-items-center ion-justify-content-center>
                  { tap ?
                      <IonList className="ion-text-center centering backgroundColor">
                        { absence =='true' ?
                          <IonList  className="ion-text-center" >
                            <IonItem lines="none">
                              <IonLabel className="text-green">Clock In Success</IonLabel>
                            </IonItem>
                            <IonItem lines="none">
                              <IonLabel className="text-green">Happy Working</IonLabel>
                            </IonItem>
                          </IonList>:
                          <IonList  className="ion-text-center" >
                            <IonItem lines="none">
                              <IonLabel className="text-green">Clock Out Success</IonLabel>
                            </IonItem>
                            <IonItem lines="none">
                              <IonLabel className="text-green">See You Tomorrow</IonLabel>
                            </IonItem>                  
                          </IonList>
                        } 
                      </IonList>
                        :
                        null
                    }
                </IonCol> 
                <IonCol size='12' ion-align-items-center ion-justify-content-center>
              {
                  maxClockIn == true ?

                  <IonCard  onClick={() => setShowAlert(true)} type="button" className="clock-card ion-text-center clockCardGray"> 
                    <IonAlert
                      isOpen={showAlert}
                      onDidDismiss={() => setShowAlert(false)}
                      cssClass='my-custom-class'
                      header={'Alert'}
                      subHeader={''}
                      message={'You Already Reached the Limit Of Clock In, Please Try Again Tomorrow'}
                      buttons={['OK']}
                      // animated={true}
                    />        
                    <IonCardHeader className="clockTitle gray">Tap To Clock In</IonCardHeader>
                    <IonCardContent className="clock gray">{clock.toLocaleTimeString()}</IonCardContent>
                  </IonCard> :
                  <IonCard onClick={tapClock} type="button" className={`clock-card ion-text-center ${clockColor}`}>          
                    <IonCardHeader className={`clockTitle ${color}`}>{absence == 'true' ? 'Tap to Clock Out' : 'Tap To Clock In'}</IonCardHeader>
                    <IonCardContent className={`clock ${color}`}>{clock.toLocaleTimeString()}</IonCardContent>
                  </IonCard>
                }  
              </IonCol>      
            </IonRow>
          </IonGrid>
                :
              <Redirect to="/Login" />
              
            }
        </IonContent>
      </IonPage>
  )
}

export default Home;
