export const loginAction = (payload: any) => {

  localStorage.setItem('user',JSON.stringify(payload))
  return { type: 'LOGIN_SUCCESS', payload }
}

export const onLogout = () => {
  localStorage.removeItem('user')
  return { type: 'LOGOUT_SUCCESS' }
} 

export const tapIn = (payload: any) => {
  const user = JSON.parse(localStorage.getItem('user') || 'null')
  user.attendance = 'true'
  localStorage.setItem('user',JSON.stringify(user))
  return { type: 'TAP_IN_SUCCESS', payload }
}

export const tapOut = (payload: any) => {
  const user = JSON.parse(localStorage.getItem('user') || 'null')
  user.attendance = 'false'

  localStorage.setItem('user',JSON.stringify(user))
  return { type: 'TAP_OUT_SUCCESS', payload }
} 