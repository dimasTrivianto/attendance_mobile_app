const defaultState = {
  email: "",
  token : "",
  attendance : ""
}


export default function reducer(
  state = defaultState, 
  { type, payload }: {type: string, payload: any}
  ) : any {
  switch(type) {
      case 'LOGIN_SUCCESS':
          return {...state, email: payload.user.email, token: payload.token, attendance: payload.attendance  }
      case 'LOGOUT_SUCCESS':
          return {...defaultState}    
      case 'TAP_IN_SUCCESS' :
          return {...state, attendance: payload.attendance }    
      case 'TAP_OUT_SUCCESS' :
          return {...state, attendance: payload.attendance }    
      default :
          return state
  }
}

