import axios from 'axios'

export default axios.create({
    baseURL: 'https://api-zukses.yokesen.com'
    // baseURL: 'https://4a97af3320dd.ngrok.io'
})